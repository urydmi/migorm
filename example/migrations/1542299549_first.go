package migrations

import (
	"errors"

	"gorm.io/gorm"

	"gitlab.com/urydmi/migorm"
)

func init() {
	migorm.RegisterMigration(&migrationFirst{})
}

type migrationFirst struct{}

func (m *migrationFirst) Up(db *gorm.DB, log migorm.Logger) error {

	err := errors.New("implement me")

	return err
}

func (m *migrationFirst) Down(db *gorm.DB, log migorm.Logger) error {

	err := errors.New("implement me")

	return err
}
